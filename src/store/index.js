import { createStore } from 'vuex'
import axios from 'axios'

const store = createStore({
  state: {
    cart: [],
    user: JSON.parse(localStorage.getItem('dataUser')) || null,
  },
  mutations: {
    setCart(state, payload) {
      let productInCart = state.cart.find(item => {
        return item.product.id == payload.product.id;
      })

      if(productInCart) {
        productInCart.quantity += payload.quantity;
        return;
      }

      state.cart.push(payload)
    },
    setUser(state, payload) {
      state.user = payload;
    },
    setCarts(state, payload) {
      state.products.push(payload);
    },
    decreaseQuantity(state, id) {
      state.cart.find((item) => {
        if(item.product.id == id) {
          item.quantity -= 1
        }
      })
    },
    increaseQuantity(state, id) {
      state.cart.find((item) => {
        if(item.product.id == id) {
          item.quantity += 1
        }
      })
    },
    empty(state){
      state.cart = [];
    }
  },
  actions: {

    async addProductToCart(context, {product, quantity}) {
      context.commit('setCart', {product, quantity});
    },
    async postProducts(context, {id, quantity}) {
      context.commit('setCarts', {id, quantity});
      try {
        const res = await axios.post('https://dummyjson.com/carts/add', {
          userId: 1,
          products: [
            {
              id: 1,
              quantity: 1
            },
            {
              id: 50,
              quantity: 2
            },
          ]
        })
        console.log(res.data);
      } catch (error) {
          console.log(error);
      }
    },
    login(context, {username, password}) {
      context.commit('setUser', {username, password});
      localStorage.setItem('dataUser', JSON.stringify({username, password}));
    },
    logout(context) {
      context.commit('setUser', null);
      localStorage.setItem('dataUser', null);
    }
  },
  getters: {
    cartItemCount(state){
      return state.cart.length;
    },
    cartTotalPrice(state) {
      let total = 0;

      state.cart.forEach(item => {
        total += (item.product.price * item.quantity);
      })

      return total;
    }
  },
  productId(state){
    state.cart.filter(item => item.product.id);
  },
  productQty(state){
    state.cart.filter(item => item.quantity);
  },
  quantityPd(state, getters){
    return getters.productQty.length;
  }
})

export default store