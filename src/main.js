import { createApp } from 'vue'
import store from './store/index'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.js"
import 'bootstrap-icons/font/bootstrap-icons.css'
import vue3GoogleLogin from 'vue3-google-login'


const app = createApp(App);

app.use(store)
app.use(vue3GoogleLogin, {
    clientId: '916220925464-t3bkp0s74v470h6c980dtoa5s03jkg8b.apps.googleusercontent.com'
})
app.use(router);
app.mount("#app");
//createApp(App).use(router).mount('#app');