import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Detail from '../views/Detail.vue'
import Category from '../views/Category.vue'
import NotFound from '../views/NotFound.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import ListCategory from '../views/ListCategory.vue'
import Authentication from '../views/Authentication.vue'
import GoogleLogin2 from '../views/GoogleLogin2.vue'
import AllCart from '../views/AllCart.vue'
import SingleCart from '../views/SingleCart.vue'


const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/categories',
    name: 'categories',
    component: ListCategory,
  },
  {
    path: '/products/:category',
    name: 'category',
    component: Category,
  },
  {
    path: '/products',
    name: 'products',
    component: NotFound
  },
  {
    path: '/products/:category/:id',
    name: 'products-detail',
    component: Detail
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/authentication',
    name: 'authentication',
    component: Authentication
  },
  {
    path: '/login-google',
    name: 'login-google',
    component: GoogleLogin2
  },
  {
    path: '/all-carts',
    name: 'cart',
    component: AllCart
  },
  {
    path: '/cart',
    name: 'single-cart',
    component: SingleCart
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
